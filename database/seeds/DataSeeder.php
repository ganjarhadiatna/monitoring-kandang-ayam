<?php

use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            // 1
            [
                'id' => '1',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            // 2
            [
                'id' => '5',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '8',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            // 3
            [
                'id' => '9',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '10',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '11',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '12',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            // 4
            [
                'id' => '13',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '14',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '15',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '16',
                'value' => 0.2,
                'description' => 'Lorem',
                'bizpar_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('datas')->insert($data);
    }
}
