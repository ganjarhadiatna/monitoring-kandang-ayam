<?php

use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Kandang Ayam 1',
                'description' => 'Lorem ipsum doler ismet',
                'user_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'name' => 'Kandang Ayam 2',
                'description' => 'Lorem ipsum doler ismet',
                'user_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'name' => 'Kandang Ayam 3',
                'description' => 'Lorem ipsum doler ismet',
                'user_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'name' => 'Kandang Ayam 4',
                'description' => 'Lorem ipsum doler ismet',
                'user_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('places')->insert($data);
    }
}
