<?php

use Illuminate\Database\Seeder;

class BizparSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'key' => 'tool_voltage',
                'value' => 'Voltage',
                'description' => 'Lorem ipsum doler ismet',
                'tool_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'key' => 'tool_currrent',
                'value' => 'Current',
                'description' => 'Lorem ipsum doler ismet',
                'tool_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'key' => 'tool_power',
                'value' => 'Power',
                'description' => 'Lorem ipsum doler ismet',
                'tool_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'key' => 'tool_frequency',
                'value' => 'Frequency',
                'description' => 'Lorem ipsum doler ismet',
                'tool_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('bizpars')->insert($data);
    }
}
