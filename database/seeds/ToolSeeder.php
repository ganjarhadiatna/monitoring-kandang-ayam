<?php

use Illuminate\Database\Seeder;

class ToolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'code' => 'TL001',
                'name' => 'Monitoring Lampu',
                'description' => 'Lorem ipsum doler ismet',
                'status' => 0,
                'place_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'code' => 'TL002',
                'name' => 'Monitoring Ketinggian Air',
                'description' => 'Lorem ipsum doler ismet',
                'status' => 0,
                'place_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'code' => 'TL003',
                'name' => 'Monitoring Kipas',
                'description' => 'Lorem ipsum doler ismet',
                'status' => 0,
                'place_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'code' => 'TL004',
                'name' => 'Monitoring Panel surya',
                'description' => 'Lorem ipsum doler ismet',
                'status' => 0,
                'place_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('tools')->insert($data);
    }
}
