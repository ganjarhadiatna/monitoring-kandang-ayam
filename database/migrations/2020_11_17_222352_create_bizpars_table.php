<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBizparsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bizpars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key', 32)->unique();
            $table->string('value', 32)->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('tool_id');
            $table->timestamps();

            $table->foreign('tool_id')->references('id')->on('tools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bizpars');
    }
}
