<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bizpar extends Model
{
    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'bizpars.id',
            'bizpars.key',
            'bizpars.value',
            'bizpars.description',
            'bizpars.tool_id',
            'bizpars.created_at',
            'bizpars.updated_at',
            'tools.name as tool_name'
        )
        ->join('tools', 'tools.id', '=', 'bizpars.tool_id')
        ->orderBy('bizpars.id', 'desc')
        ->paginate($limit, ['*'], 'page_bizpars');
    }

    public function scopeGetByToolCode($query, $code, $limit)
    {
        return $this
        ->select(
            'bizpars.id',
            'bizpars.key',
            'bizpars.value',
            'bizpars.description',
            'bizpars.tool_id',
            'bizpars.created_at',
            'bizpars.updated_at',
            'tools.name as tool_name'
        )
        ->where('tools.code', $code)
        ->join('tools', 'tools.id', '=', 'bizpars.tool_id')
        ->orderBy('bizpars.id', 'desc')
        ->paginate($limit, ['*'], 'page_bizpars');
    }

    public function scopeGetAllSearch($query, $limit, $serch)
    {
        return $this
        ->select(
            'bizpars.id',
            'bizpars.key',
            'bizpars.value',
            'bizpars.description',
            'bizpars.tool_id',
            'bizpars.created_at',
            'bizpars.updated_at',
            'tools.name as tool_name'
        )
        ->where('key', 'like', '%' . $serch . '%')
        ->orWhere('value', 'like', '%' . $serch . '%')
        ->orWhere('description', 'like', '%' . $serch . '%')
        ->join('tools', 'tools.id', '=', 'bizpars.tool_id')
        ->orderBy('bizpars.id', 'desc')
        ->paginate($limit, ['*'], 'page_bizpars');
    }
}
