<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tool extends Model
{
    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'tools.id',
            'tools.code',
            'tools.name',
            'tools.description',
            'tools.status',
            'tools.place_id',
            'tools.created_at',
            'tools.updated_at',
            'places.name as place_name',
            DB::raw('(select count(bizpars.id) from bizpars where bizpars.tool_id = tools.id) as ttl_bizpars'),
            DB::raw('(select count(datas.id) from datas join bizpars on datas.bizpar_id = bizpars.id where bizpars.tool_id = tools.id) as ttl_datas')
        )
        ->join('places', 'places.id', '=', 'tools.place_id')
        ->orderBy('tools.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllByPlaceId($query, $limit, $id)
    {
        return $this
        ->select(
            'tools.id',
            'tools.code',
            'tools.name',
            'tools.description',
            'tools.status',
            'tools.place_id',
            'tools.created_at',
            'tools.updated_at',
            'places.name as place_name',
            DB::raw('(select count(bizpars.id) from bizpars where bizpars.tool_id = tools.id) as ttl_bizpars'),
            DB::raw('(select count(datas.id) from datas join bizpars on datas.bizpar_id = bizpars.id where bizpars.tool_id = tools.id) as ttl_datas')
        )
        ->where('tools.place_id', $id)
        ->join('places', 'places.id', '=', 'tools.place_id')
        ->orderBy('tools.id', 'asc')
        ->paginate($limit);
    }

    public function scopeGetByCode($query, $code)
    {
        return $this
        ->select(
            'tools.id',
            'tools.code',
            'tools.name',
            'tools.description',
            'tools.status',
            'tools.place_id',
            'tools.created_at',
            'tools.updated_at',
            'places.name as place_name',
            DB::raw('(select count(bizpars.id) from bizpars where bizpars.tool_id = tools.id) as ttl_bizpars'),
            DB::raw('(select count(datas.id) from datas join bizpars on datas.bizpar_id = bizpars.id where bizpars.tool_id = tools.id) as ttl_datas')
        )
        ->where('code', $code)
        ->join('places', 'places.id', '=', 'tools.place_id')
        ->orderBy('tools.id', 'desc')
        ->first();
    }

    public function scopeGetAllSearch($query, $limit, $serch)
    {
        return $this
        ->select(
            'tools.id',
            'tools.code',
            'tools.name',
            'tools.description',
            'tools.status',
            'tools.place_id',
            'tools.created_at',
            'tools.updated_at',
            'places.name as place_name',
            DB::raw('(select count(bizpars.id) from bizpars where bizpars.tool_id = tools.id) as ttl_bizpars'),
            DB::raw('(select count(datas.id) from datas join bizpars on datas.bizpar_id = bizpars.id where bizpars.tool_id = tools.id) as ttl_datas')
        )
        ->where('tools.name', 'like', '%' . $serch . '%')
        ->orWhere('tools.description', 'like', '%' . $serch . '%')
        ->orWhere('tools.code', 'like', '%' . $serch . '%')
        ->orWhere('tools.status', 'like', '%' . $serch . '%')
        ->join('places', 'places.id', '=', 'tools.place_id')
        ->orderBy('tools.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllSearchByID($query, $limit, $id, $serch)
    {
        return $this
        ->select(
            'tools.id',
            'tools.code',
            'tools.name',
            'tools.description',
            'tools.status',
            'tools.place_id',
            'tools.created_at',
            'tools.updated_at',
            'places.name as place_name',
            DB::raw('(select count(bizpars.id) from bizpars where bizpars.tool_id = tools.id) as ttl_bizpars'),
            DB::raw('(select count(datas.id) from datas join bizpars on datas.bizpar_id = bizpars.id where bizpars.tool_id = tools.id) as ttl_datas')
        )
        // ->where('tools.place_id', $id)
        ->where('tools.name', 'like', '%' . $serch . '%')
        ->orWhere('tools.description', 'like', '%' . $serch . '%')
        ->orWhere('tools.code', 'like', '%' . $serch . '%')
        ->orWhere('tools.status', 'like', '%' . $serch . '%')
        ->join('places', 'places.id', '=', 'tools.place_id')
        ->orderBy('tools.id', 'desc')
        ->paginate($limit);
    }
}
