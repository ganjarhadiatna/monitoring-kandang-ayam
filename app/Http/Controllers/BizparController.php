<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Place;
use App\Bizpar;
use Carbon\Carbon;
use Auth;
use DB;

class BizparController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Bizpar::GetAll(10);
        return view('bizpar.index', ['data' => $data, 'title' => 'Data Parameter']);
    }

    public function search()
    {
        $serch = $_GET['search'];
        $data = Bizpar::GetAllSearch(10, $serch);
        return view('bizpar.index', ['data' => $data, 'title' => 'Pencarian: ' . $serch]);
    }

    public function create()
    {
        $place = Tool::get();
        return view('bizpar.form', ['place' => $place]);
    }

    public function createById($place_id, $tool_id)
    {
        $tools = Tool::where('id', $tool_id)->get();
        return view('bizpar.form', ['place' => $tools, 'sidebar' => 'place-' . $place_id, 'id' => $place_id]);
    }

    public function edit($id)
    {
        $bizpar = Bizpar::where(['id' => $id])->first();
        $tool = Tool::where(['id' => $bizpar->tool_id])->first();
        $tools = Tool::where('id', $bizpar->tool_id)->get();
        return view('bizpar.form', ['data' => $bizpar, 'place' => $tools, 'sidebar' => 'place-' . $tool->place_id, 'id' => $id]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'key' => 'required|min:0|max:32|unique:bizpars',
            'value' => 'min:0|max:32',
            'description' => 'min:0|max:255',
            'tool_id' => 'required'
        ]);

        $tool_id = $request->input('tool_id');

        $data = [
            'key' => $request->input('key'),
            'value' => $request->input('value'),
            'description' => $request->input('description'),
            'tool_id' => $tool_id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Bizpar::insert($data);

        if ($service) 
        {
            $tool = Tool::where('id', $tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=parameter');
        }
        else 
        {
            $tool = Tool::where('id', $tool_id)->first();
            return redirect('/bizpar/create/'.$tool->place_id.'/'.$tool_id);
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'value' => 'min:0|max:32',
            'description' => 'min:0|max:255',
            'tool_id' => 'required'
        ]);

        $id = $request->input('id');
        $tool_id = $request->input('tool_id');

        $data = [
            'value' => $request->input('value'),
            'description' => $request->input('description'),
            'tool_id' => $tool_id,
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Bizpar::where(['id' => $id])->update($data);

        if ($service) 
        {
            $tool = Tool::where('id', $tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=parameter');
        }
        else 
        {
            return redirect('/bizpar/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');
        $tool_id = Bizpar::where('id', $id)->first();

        $service = Bizpar::where(['id' => $id])->delete();

        if ($service) 
        {
            $tool = Tool::where('id', $tool_id->tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=parameter');
        }
        else 
        {
            $tool = Tool::where('id', $tool_id->id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=parameter');
        }
    }
}
