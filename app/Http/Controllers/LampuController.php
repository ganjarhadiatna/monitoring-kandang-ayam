<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lampu;
use App\Monitoring;
use App\Charts\MonitoringChart;
use Carbon\Carbon;
use Auth;
use DB;

class LampuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Lampu::GetAll(10);
        return view('lampu.index', ['data' => $data, 'title' => 'Data Lampu']);
    }

    public function search()
    {
        $serch = $_GET['search'];
        $data = Lampu::GetAllSearch(10, $serch);
        return view('lampu.index', ['data' => $data, 'title' => 'Pencarian: ' . $serch]);
    }

    public function create()
    {
        return view('lampu.form');
    }

    public function edit($id)
    {
        $data = Lampu::where(['id' => $id])->first();
        return view('lampu.form', ['data' => $data]);
    }

    public function monitoring($id)
    {
        // $fillColors = "rgba(255, 99, 132, 0.2)";
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];

        $profile = Lampu::where('id', $id)->first();
        $data = Monitoring::GetByIDLampu(10, $id);

        $mnTegangan = [];
        $mnArus = [];
        $mnDaya = [];
        $mnFrekuensi = [];
        $mnDayaFaktor = [];
        $tgl = Carbon::now()->day();
        $title = ['Tegangan', 'Arus', 'Daya', 'Frekuensi', 'Daya Faktor'];
        $tanggal = [];
        $monitoring = Monitoring::where('lampu_id', $id)->whereDay('created_at', '<=', $tgl)->limit(5)->orderBy('id', 'desc')->get();
        foreach ($monitoring as $dt) {
            array_push($mnTegangan, $dt->tegangan);
            array_push($mnArus, $dt->arus);
            array_push($mnDaya, $dt->daya);
            array_push($mnFrekuensi, $dt->frekuensi);
            array_push($mnDayaFaktor, $dt->daya_faktor);
            array_push($tanggal, [date_format($dt->created_at, "d/m/y")]);
        }

        $chMonitoring = new MonitoringChart();
        // $chMonitoring->title('Data Monitoring', 20, "rgb(255, 99, 132)", true);
        $chMonitoring->labels($tanggal);
        $chMonitoring->barwidth(0.5);
        $chMonitoring->dataset('Tegangan', 'bar', $mnTegangan)->backgroundcolor($fillColors[0]);
        $chMonitoring->dataset('Arus', 'bar', $mnArus)->backgroundcolor($fillColors[1]);
        $chMonitoring->dataset('Daya', 'bar', $mnDayaFaktor)->backgroundcolor($fillColors[2]);
        $chMonitoring->dataset('Frekuensi', 'bar', $mnFrekuensi)->backgroundcolor($fillColors[3]);
        $chMonitoring->dataset('Status Baterai', 'bar', $mnDayaFaktor)->backgroundcolor($fillColors[6]);

        return view('monitoring.index', [
            'profile' => $profile,
            'data' => $data, 
            'id' => $id, 
            'chMonitoring' => $chMonitoring,
        ]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|min:4|max:4',
            'nama' => 'required|min:0|max:150',
            'lokasi' => 'required|min:0|max:255',
            'longitude' => 'min:0|max:255',
            'latitude' => 'min:0|max:255',
            'status' => 'required'
        ]);

        $data = [
            'kode' => $request->input('kode'),
            'nama' => $request->input('nama'),
            'lokasi' => $request->input('lokasi'),
            'longitude' => $request->input('longitude'),
            'latitude' => $request->input('latitude'),
            'status' => $request->input('status'),
            'users_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Lampu::insert($data);

        if ($service) 
        {
            return redirect('/lampu');
        }
        else 
        {
            return redirect('/lampu/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'lokasi' => 'required|min:0|max:255',
            'longitude' => 'min:0|max:255',
            'latitude' => 'min:0|max:255',
            'status' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'lokasi' => $request->input('lokasi'),
            'longitude' => $request->input('longitude'),
            'latitude' => $request->input('latitude'),
            'status' => $request->input('status'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Lampu::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/lampu');
        }
        else 
        {
            return redirect('/lampu/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Lampu::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/lampu');
        }
        else 
        {
            return redirect('/lampu');
        }
    }
}
