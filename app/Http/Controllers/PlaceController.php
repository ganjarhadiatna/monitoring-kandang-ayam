<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Tool;
use Carbon\Carbon;
use Auth;
use DB;

class PlaceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Place::GetAll(10);
        return view('place.index', ['data' => $data, 'title' => 'Kelola Tempat', 'sidebar' => 'place']);
    }

    public function search()
    {
        $serch = $_GET['search'];
        $data = Place::GetAllSearch(10, $serch);
        return view('place.index', ['data' => $data, 'title' => 'Pencarian: ' . $serch, 'sidebar' => 'place']);
    }

    public function create()
    {
        return view('place.form', ['sidebar' => 'place']);
    }

    public function detail($id)
    {
        $data = Place::GetById($id);
        $tools = Tool::GetAllByPlaceId(3, $data->id);
        return view('place.detail', [
            'title' => 'Tempat : ' . $data->name,
            'subtitle' => $data->description,
            'sidebar' => 'place-' . $data->id,
            'tools' => $tools,
            'id' => $id
        ]);
    }

    public function edit($id)
    {
        $data = Place::where(['id' => $id])->first();
        return view('place.form', ['data' => $data, 'sidebar' => 'place-' . $data->id]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:0|max:32',
            'description' => 'min:0|max:255'
        ]);

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Place::insert($data);

        if ($service) 
        {
            $data = Place::orderBy('id', 'desc')->first();
            return redirect('/place/detail/'.$data->id);
        }
        else 
        {
            return redirect('/place/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|min:0|max:32',
            'description' => 'min:0|max:255'
        ]);

        $id = $request->input('id');

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Place::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/place/detail/'.$id);
        }
        else 
        {
            return redirect('/place/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Place::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/home');
        }
        else 
        {
            return redirect('/home');
        }
    }
}
