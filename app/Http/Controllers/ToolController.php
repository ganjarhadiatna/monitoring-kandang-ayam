<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Place;
use App\Bizpar;
use App\Data;
use Carbon\Carbon;
use Auth;
use DB;

class ToolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Tool::GetAll(10);
        return view('tools.index', ['data' => $data, 'title' => 'Kelola Alat', 'sidebar' => 'tool']);
    }

    public function detail($id, $code)
    {
        $data = Tool::GetByCode($code);
        $params = Bizpar::GetByToolCode($code, 10);
        $monitoring = Data::GetByToolCode($code, 10);
        return view('tools.detail', [
            'data' => $data, 
            'title' => 'Alat : ' . $data->name,
            'params' => $params,
            'monitoring' => $monitoring,
            'sidebar' => 'place-' . $id,
            'id' => $id
        ]);
    }

    public function search()
    {
        $serch = $_GET['search'];
        $data = Tool::GetAllSearch(10, $serch);
        return view('tools.index', ['data' => $data, 'title' => 'Pencarian: ' . $serch, 'sidebar' => 'tool']);
    }

    public function searchByID($id)
    {
        $serch = $_GET['search'];
        $data = Place::GetById($id);
        $tools = Tool::GetAllSearchByID(3, $data->id, $serch);
        return view('place.detail', [
            'title' => 'Tempat : ' . $data->name,
            'subtitle' => $data->description,
            'sidebar' => 'place-' . $data->id,
            'tools' => $tools,
            'id' => $id
        ]);
    }

    public function create()
    {
        $place = Place::get();
        return view('tools.form', ['place' => $place, 'sidebar' => 'tool']);
    }

    public function createById($id)
    {
        $place = Place::get();
        return view('tools.form', ['place' => $place, 'sidebar' => 'place-' . $id, 'id' => $id]);
    }

    public function edit($id)
    {
        $place = Place::get();
        $data = Tool::where(['id' => $id])->first();
        return view('tools.form', ['data' => $data, 'place' => $place, 'sidebar' => 'tool']);
    }

    public function editById($place_id, $id)
    {
        $place = Place::get();
        $data = Tool::where(['id' => $id])->first();
        return view('tools.form', ['data' => $data, 'place' => $place, 'sidebar' => 'place-' . $place_id, 'place_id' => $place_id]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|min:5|max:5|unique:tools',
            'name' => 'required|min:0|max:32',
            'description' => 'min:0|max:255',
            'status' => 'required',
            'place_id' => 'required'
        ]);

        $place_id = $request->input('place_id');

        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'place_id' => $place_id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Tool::insert($data);

        if ($service) 
        {
            return redirect('/place/detail/'.$place_id);
        }
        else 
        {
            return redirect('/tool/create/'.$place_id);
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|min:0|max:32',
            'description' => 'min:0|max:255',
            'status' => 'required',
            'place_id' => 'required'
        ]);

        $id = $request->input('id');
        $code = $request->input('code');
        $place_id = $request->input('place_id');

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'place_id' => $place_id,
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Tool::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/place/detail/'.$place_id.'/tool/'.$code);
        }
        else 
        {
            return redirect('/tool/edit/'.$place_id.'/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');
        $place_id = Tool::where(['id' => $id])->first();
        $service = Tool::where(['id' => $id])->delete();

        if ($service) 
        {
            // return redirect('/tool');
            return redirect('/place/detail/'.$place_id->place_id);
        }
        else 
        {
            // return redirect('/tool');
            return redirect('/place/detail/'.$place_id->place_id);
        }
    }
}
