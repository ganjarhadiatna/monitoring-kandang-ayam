<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Bizpar;
use App\Data;

class ApiController extends Controller
{
    public function post(Request $request)
    {
        $data = $request->json()->all();
        $code = $data['code'];
        $about = $data['about'];
        $temp = $data['temp'];
        $humidity = $data['humidity'];
        $tool = Tool::where('code', $code)->first();

        $tool_about = Bizpar::where('key', $about['code'])->first();
        $tool_temp = Bizpar::where('key', $temp['code'])->first();
        $tool_humidity = Bizpar::where('key', $humidity['code'])->first();

        $payload = [
            [
                'value' => $about['value'],
                'description' => 'Lorem',
                'bizpar_id' => $tool_about->id,
            ],
            [
                'value' => $temp['value'],
                'description' => 'Lorem',
                'bizpar_id' => $tool_temp->id,
            ],
            [
                'value' => $humidity['value'],
                'description' => 'Lorem',
                'bizpar_id' => $tool_humidity->id,
            ]
        ];
        $service = Data::insert($payload);
        if ($service) 
        {
            echo json_encode([
                'code' => 200,
                'status' => 'S',
                'message' => 'data saved'
            ]);
        } else {
            echo json_encode([
                'code' => 202,
                'status' => 'F',
                'message' => 'data not saved'
            ]);
        }
    }

    public function get()
    {
        $data = [
            'status' => 'nyala'
        ];
        echo json_encode($data);
    }

    public function status(Request $request)
    {
        $id = $request->input('id');
        $data = Tool::where(['id' => $id])->first();

        if ($data->status == '0')
        {
            Tool::where(['id' => $id])->update(['status' => '1']);

            $out = [
                'message' => 'Hidup',
                'status' => 'ok',
                'status_tool' => '1',
                'code' => 201
            ];
        }
        else 
        {
            Tool::where(['id' => $id])->update(['status' => '0']);

            $out = [
                'message' => 'Mati',
                'status' => 'ok',
                'status_tool' => '0',
                'code' => 201
            ];
        }

        return response()->json($out, $out['code']);
    }
}
