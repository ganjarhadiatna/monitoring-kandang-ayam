<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;

class TestController extends Controller
{
    public function save(Request $request)
    {
        $data = [
            'id' => $request->input('id'),
            'tegangan' => $request->input('tegangan'),
            'arus' => $request->input('arus'),
            'daya' => $request->input('daya'),
            'frekuensi' => $request->input('frekuensi'),
            'daya_faktor' => $request->input('daya_faktor')
        ];

        $service = Data::insert($data);

        if ($service) 
        {
            $out = [
                'message' => 'success to save data',
                'status' => 'ok',
                'data' => $data,
                'code' => 201
            ];
        }
        else 
        {
            $out = [
                'message' => 'failed to save data',
                'status' => 'error',
                'code' => 401
            ];
        }

        return response()->json($out, $out['code']);
    }

    public function get()
    {
        $data = [
            'title' => 'ahuy'
        ];
        $out = [
            'message' => 'success',
            'status' => 'ok',
            'data' => $data,
            'code' => 201
        ];
        return response()->json($out, $out['code']);
    }
}
