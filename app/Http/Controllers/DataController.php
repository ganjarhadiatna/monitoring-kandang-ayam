<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Tool;
use App\Bizpar;
use App\Data;
use Carbon\Carbon;
use Auth;
use DB;

class DataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Data::GetAll(10);
        return view('data.index', ['data' => $data, 'title' => 'Data Monitoring', 'sidebar' => 'data']);
    }

    public function search()
    {
        $serch = $_GET['search'];
        $data = Data::GetAllSearch(10, $serch);
        return view('data.index', ['data' => $data, 'title' => 'Pencarian: ' . $serch, 'sidebar' => 'data']);
    }

    public function create()
    {
        $place = Bizpar::get();
        return view('data.form', ['place' => $place, 'sidebar' => 'data']);
    }

    public function createById($place_id, $tool_id)
    {
        $tools = Tool::where('id', $tool_id)->first();
        $bizpars = Bizpar::where('tool_id', $tools->id)->get();
        return view('data.form', ['place' => $bizpars, 'sidebar' => 'place-' . $place_id, 'id' => $place_id]);
    }

    public function edit($id)
    {
        $data = Data::where(['id' => $id])->first();
        $bizpar = Bizpar::where('id', $data->bizpar_id)->first();
        $tool = Tool::where('id', $bizpar->tool_id)->first();
        $bizpars = Bizpar::where('tool_id', $tool->id)->get();
        return view('data.form', ['data' => $data, 'place' => $bizpars, 'sidebar' => 'place-' . $tool->place_id, 'id' => $id]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|min:0|max:32',
            'description' => 'min:0|max:255',
            'bizpar_id' => 'required'
        ]);

        $bizpar_id = $request->input('bizpar_id');

        $data = [
            'value' => $request->input('value'),
            'description' => $request->input('description'),
            'bizpar_id' => $bizpar_id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Data::insert($data);

        if ($service) 
        {
            $bizpar = Bizpar::where('id', $bizpar_id)->first();
            $tool = Tool::where('id', $bizpar->tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=monitoring');
        }
        else 
        {
            return redirect('/data/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'value' => 'min:0|max:32',
            'description' => 'min:0|max:255',
            'bizpar_id' => 'required'
        ]);

        $id = $request->input('id');
        $bizpar_id = $request->input('bizpar_id');

        $data = [
            'value' => $request->input('value'),
            'description' => $request->input('description'),
            'bizpar_id' => $bizpar_id,
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Data::where(['id' => $id])->update($data);

        if ($service) 
        {
            $bizpar = Bizpar::where('id', $bizpar_id)->first();
            $tool = Tool::where('id', $bizpar->tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=monitoring');
        }
        else 
        {
            return redirect('/data/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');
        $data = Data::where('id', $id)->first();

        $service = Data::where(['id' => $id])->delete();

        if ($service) 
        {
            $bizpar = Bizpar::where('id', $data->bizpar_id)->first();
            $tool = Tool::where('id', $bizpar->tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=monitoring');
        }
        else 
        {
            $bizpar = Bizpar::where('id', $data->bizpar_id)->first();
            $tool = Tool::where('id', $bizpar->tool_id)->first();
            $place = Place::where('id', $tool->place_id)->first();
            return redirect('/place/detail/'.$place->id.'/tool/'.$tool->code.'?tabs=monitoring');
        }
    }
}
