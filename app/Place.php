<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Place extends Model
{
    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'places.id',
            'places.name',
            'places.description',
            'places.user_id',
            'places.created_at',
            'places.updated_at',
            'users.name as user_name'
        )
        ->join('users', 'users.id', '=', 'places.user_id')
        ->orderBy('places.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetById($query, $id)
    {
        return $this
        ->select(
            'places.id',
            'places.name',
            'places.description',
            'places.user_id',
            'places.created_at',
            'places.updated_at',
            'users.name as user_name'
        )
        ->where('places.id', $id)
        ->join('users', 'users.id', '=', 'places.user_id')
        ->orderBy('places.id', 'desc')
        ->first();
    }

    public function scopeGetAllSearch($query, $limit, $serch)
    {
        return $this
        ->select(
            'places.id',
            'places.name',
            'places.description',
            'places.user_id',
            'places.created_at',
            'places.updated_at',
            'users.name as user_name'
        )
        ->where('name', 'like', '%' . $serch . '%')
        ->orWhere('description', 'like', '%' . $serch . '%')
        ->join('users', 'users.id', '=', 'places.user_id')
        ->orderBy('places.id', 'desc')
        ->paginate($limit);
    }
}
