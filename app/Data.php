<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Data extends Model
{
    protected $table = 'datas';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'datas.id',
            'datas.value',
            'datas.description',
            'datas.bizpar_id',
            'datas.created_at',
            'datas.updated_at',
            'bizpars.value as bizpar_name'
        )
        ->join('bizpars', 'bizpars.id', '=', 'datas.bizpar_id')
        ->orderBy('datas.id', 'desc')
        ->paginate($limit, ['*'], 'page_datas');
    }

    public function scopeGetByToolCode($query, $code, $limit)
    {
        return $this
        ->select(
            'datas.id',
            'datas.value',
            'datas.description',
            'datas.bizpar_id',
            'datas.created_at',
            'datas.updated_at',
            'bizpars.value as bizpar_name'
        )
        ->where('tools.code', $code)
        ->join('bizpars', 'bizpars.id', '=', 'datas.bizpar_id')
        ->join('tools', 'tools.id', '=', 'bizpars.tool_id')
        ->orderBy('datas.id', 'desc')
        ->paginate($limit, ['*'], 'page_datas');
    }

    public function scopeGetAllSearch($query, $limit, $serch)
    {
        return $this
        ->select(
            'datas.id',
            'datas.value',
            'datas.description',
            'datas.bizpar_id',
            'datas.created_at',
            'datas.updated_at',
            'bizpars.value as bizpar_name'
        )
        ->where('datas.value', 'like', '%' . $serch . '%')
        ->orWhere('datas.description', 'like', '%' . $serch . '%')
        // ->orWhere('bizpars.name', 'like', '%' . $serch . '%')
        ->join('bizpars', 'bizpars.id', '=', 'datas.bizpar_id')
        ->orderBy('datas.id', 'desc')
        ->paginate($limit, ['*'], 'page_datas');
    }
}
