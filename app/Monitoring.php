<?php   
namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitoring extends Model{
    protected $table = "monitoring";

    // protected $fillable = [];

    // public $timestamps = false;

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'lampu.id',
            'lampu.kode',
            'lampu.nama',
            'lampu.lokasi',
            'lampu.longitude',
            'lampu.latitude',
            'lampu.status',
            'monitoring.id as id_m',
            'monitoring.tegangan',
            'monitoring.arus',
            'monitoring.daya',
            'monitoring.frekuensi',
            'monitoring.daya_faktor',
            'monitoring.lampu_id',
            'lampu.created_at',
            'lampu.updated_at'
        )
        ->join('lampu', 'lampu.id', '=', 'monitoring.lampu_id')
        ->orderBy('id_m', 'desc')
        ->paginate($limit);
    }

    public function scopeGetByIDLampu($query, $limit, $id)
    {
        return $this
        ->select(
            'lampu.id',
            'lampu.kode',
            'lampu.nama',
            'lampu.lokasi',
            'lampu.longitude',
            'lampu.latitude',
            'lampu.status',
            'monitoring.id as id_m',
            'monitoring.tegangan',
            'monitoring.arus',
            'monitoring.daya',
            'monitoring.frekuensi',
            'monitoring.daya_faktor',
            'monitoring.lampu_id',
            'lampu.created_at',
            'lampu.updated_at'
        )
        ->where('monitoring.lampu_id', $id)
        ->join('lampu', 'lampu.id', '=', 'monitoring.lampu_id')
        ->orderBy('id_m', 'desc')
        ->paginate($limit);
    }
}