<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'place'], function () {
    // ui
    Route::get('', 'PlaceController@index')->name('ui-place');
    Route::get('/search', 'PlaceController@search')->name('ui-place-search');
    Route::get('/create', 'PlaceController@create')->name('ui-place-create');
    Route::get('/edit/{id}', 'PlaceController@edit')->name('ui-place-edit');
    Route::get('/detail/{id}', 'PlaceController@detail')->name('ui-place-detail');
    Route::get('/detail/{id}/tool/{code}', 'ToolController@detail')->name('ui-tool-detail');
    Route::get('/detail/{id}/search', 'ToolController@searchByID')->name('ui-place-detail-tool-search');
    Route::get('/monitoring/{id}', 'PlaceController@monitoring')->name('ui-place-monitoring');

    // crud
    Route::post('/save', 'PlaceController@save')->name('ui-place-save');
    Route::post('/update', 'PlaceController@update')->name('ui-place-update');
    Route::post('/delete', 'PlaceController@delete')->name('ui-place-delete');
});

Route::group(['prefix' => 'tool'], function () {
    // ui
    Route::get('', 'ToolController@index')->name('ui-tool');
    Route::get('/search', 'ToolController@search')->name('ui-tool-search');
    Route::get('/create', 'ToolController@create')->name('ui-tool-create');
    Route::get('/create/{id}', 'ToolController@createById')->name('ui-tool-create-by-id');
    Route::get('/edit/{id}', 'ToolController@edit')->name('ui-tool-edit');
    Route::get('/edit/{pl_id}/{id}', 'ToolController@editById')->name('ui-tool-edit-by-id');
    Route::get('/monitoring/{id}', 'ToolController@monitoring')->name('ui-tool-monitoring');

    // crud
    Route::post('/save', 'ToolController@save')->name('ui-tool-save');
    Route::post('/update', 'ToolController@update')->name('ui-tool-update');
    Route::post('/delete', 'ToolController@delete')->name('ui-tool-delete');
});

Route::group(['prefix' => 'bizpar'], function () {
    // ui
    Route::get('', 'BizparController@index')->name('ui-bizpar');
    Route::get('/search', 'BizparController@search')->name('ui-bizpar-search');
    Route::get('/create', 'BizparController@create')->name('ui-bizpar-create');
    Route::get('/create/{place_id}/{tool_id}', 'BizparController@createById')->name('ui-toolbizpar-create-by-id');
    Route::get('/edit/{id}', 'BizparController@edit')->name('ui-bizpar-edit');
    Route::get('/monitoring/{id}', 'BizparController@monitoring')->name('ui-bizpar-monitoring');

    // crud
    Route::post('/save', 'BizparController@save')->name('ui-bizpar-save');
    Route::post('/update', 'BizparController@update')->name('ui-bizpar-update');
    Route::post('/delete', 'BizparController@delete')->name('ui-bizpar-delete');
});

Route::group(['prefix' => 'data'], function () {
    // ui
    Route::get('', 'DataController@index')->name('ui-data');
    Route::get('/search', 'DataController@search')->name('ui-data-search');
    Route::get('/create', 'DataController@create')->name('ui-data-create');
    Route::get('/edit/{id}', 'DataController@edit')->name('ui-data-edit');
    Route::get('/create/{place_id}/{tool_id}', 'DataController@createById')->name('ui-data-create-by-id');

    // crud
    Route::post('/save', 'DataController@save')->name('ui-data-save');
    Route::post('/update', 'DataController@update')->name('ui-data-update');
    Route::post('/delete', 'DataController@delete')->name('ui-data-delete');
    Route::post('/delete-all', 'DataController@deleteAll')->name('ui-data-delete-all');
});
