@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper" style="margin-bottom: 10px;">
                <div class="mr-auto">
                    <h3 style="margin-top: 6px;">{{ $title }}</h3>
                    <p>{{ $subtitle }}</p>
                </div>
                <div class="ml-auto">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                        <i class="fa fa-lw fa-trash-alt"></i>
                    </button>
                    <a href="{{ route('ui-place-edit', $id) }}" class="btn btn-warning">
                        <i class="fa fa-lw fa-pencil-alt"></i>
                    </a>
                    <a href="{{ route('ui-tool-create-by-id', $id) }}" class="btn btn-primary">
                        <i class="fa fa-lw fa-plus"></i> Tambah Alat
                    </a>
                </div>
            </div>
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel-{{ $id }}" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header"> 
                        <h5 class="modal-title" id="deleteModalLabel-{{ $id }}">Peringatan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Data akan dihapus secara permanen, lanjutkan?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <a class="btn btn-primary" 
                            href="{{ route('ui-place-delete') }}"
                            onclick="event.preventDefault(); document.getElementById('id-form-{{ $id }}').submit();"
                            >
                            Lanjutkan
                        </a>

                        <form id="id-form-{{ $id }}" action="{{ route('ui-place-delete') }}" method="POST" style="display: block;">
                            @csrf
                            <input type="hidden" name="id" value="{{ $id }}" />
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        @if(count($tools) == 0)
        <div class="col-md-12">
            <h5 style="margin-bottom: 30px; margin-left: 15px;">Daftar Alat Kosong</h5>
        </div>
        @else
        <div class="col-md-12">
            <div class="wrapper" style="margin-bottom: 30px;">
                <div class="mr-auto">
                    <h5 style="margin-top: 5px; margin-left: 15px;">Daftar Alat</h5>
                </div>
                <div class="ml-auto">
                    <form mtehod="GET" action="{{ route('ui-place-detail-tool-search', $id) }}" style="width: 500px; display: inline-block; vertical-align: top; margin-right: 15px;">
                        <input id="search" type="search" class="form-control @error('search') is-invalid @enderror" name="search" value="{{ isset($_GET['search']) ? $_GET['search'] : old('searc') }}" required placeholder="Cari berdasarkan kode, nama atau deskripsi.." autocomplete="search">
                    </form>
                </div>
            </div>
            <div class="wrapper" style="margin-bottom: 15px;">    
                <?php $i = 0; ?>
                @foreach($tools as $dt)
                    <div class="card card-hover" style="margin: 0 15px; width: 100%;">
                        <div class="card-body">
                            <a href="{{ route('ui-tool-detail', [$id, $dt->code]) }}">
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <b class="mr-auto">KODE</b>
                                    <b class="ml-auto">{{ $dt->code }}</b>
                                </div>
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <div class="mr-auto">Nama</div>
                                    <div class="ml-auto">{{ $dt->name }}</div>
                                </div>
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <div class="mr-auto">Deskripsi</div>
                                    <div class="ml-auto">{{ $dt->description }}</div>
                                </div>
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <div class="mr-auto">Parameter</div>
                                    <div class="ml-auto">{{ $dt->ttl_bizpars }}</div>
                                </div>
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <div class="mr-auto">Monitoring</div>
                                    <div class="ml-auto">{{ $dt->ttl_datas }}</div>
                                </div>
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <div class="mr-auto">Tgl dibuat</div>
                                    <div class="ml-auto">{{ $dt->created_at }}</div>
                                </div>
                                <div class="wrapper" style="margin-bottom: 20px;">
                                    <div class="mr-auto">Tgl diedit</div>
                                    <div class="ml-auto">{{ $dt->updated_at }}</div>
                                </div>
                            </a>
                            <div class="wrapper" style="margin-bottom: 0; border-top: 0.5px #ccc solid; padding-top: 20px;">
                                <div class="mr-auto">
                                    <div style="margin-top: 7px;">Status</div>
                                </div>
                                <div class="ml-auto">
                                    @if($dt->status == 0) 
                                        <button id="button-tool-{{ $dt->id }}" class="btn btn-danger" onclick="setStatus({{ $dt->id }})">
                                            <i id="button-icon-{{ $dt->id }}" class="fa fa-lw fa-minus"></i>
                                        </button>
                                    @endif 
                                    @if($dt->status == 1) 
                                        <button id="button-tool-{{ $dt->id }}" class="btn btn-success" onclick="setStatus({{ $dt->id }})">
                                            <i id="button-icon-{{ $dt->id }}" class="fa fa-lw fa-check"></i>
                                        </button>
                                    @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                @endforeach
            </div>
            <div class="wrapper">
                <div class="mr-auto" style="margin-top: 30px; margin-left: 15px;">
                    {{ $tools->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
</div>

<script>
    function setStatus (id) {
        $.ajax({
            url: "{{ url('api/status') }}",
            dataType: 'json',
            type: 'POST',
            data: {
                'id': id
            }
        }).done(function(data) {
            console.log('data', data)
            var dt = data.status_tool;
            if (dt == '1') {
                $('#button-tool-' + id).removeClass('btn btn-danger').addClass('btn btn-success');
                $('#button-icon-' + id).removeClass('fa fa-lw fa-minus').addClass('fa fa-lw fa-check');
            } else {
                $('#button-tool-' + id).removeClass('btn btn-success').addClass('btn btn-danger');
                $('#button-icon-' + id).removeClass('fa fa-lw fa-check').addClass('fa fa-lw fa-minus');
            }
        });
    }
</script>

@endsection
