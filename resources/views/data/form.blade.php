@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Monitoring</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Monitoring</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-data-update') }} @else {{ route('ui-data-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <label for="inputValue">Nilai</label>
                            <input type="text" value="@if(isset($data)) {{ $data->value }} @endif" class="form-control @error('value') is-invalid @enderror" id="inputValue" name="value" required>
                            @error('value')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputDescription">Deskripsi</label>
                            <input type="text" value="@if(isset($data)) {{ $data->description }} @endif" class="form-control @error('description') is-invalid @enderror" id="inputDescription" name="description">
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputBizparId">Pilih Parameter</label>
                                <select value="@if(isset($data)) {{ $data->bizpar_id }} @endif" class="form-control @error('bizpar_id') is-invalid @enderror" id="inputBizparId" name="bizpar_id">
                                    @foreach($place as $pl)
                                        @if(isset($data))
                                            @if($pl->id == $data->bizpar_id)
                                                <option value="{{ $pl->id }}" selected>{{ $pl->value }}</option>
                                            @else
                                                <option value="{{ $pl->id }}">{{ $pl->value }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $pl->id }}">{{ $pl->value }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @error('tool_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
