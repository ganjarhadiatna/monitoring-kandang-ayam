@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Alat</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Alat</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-tool-update') }} @else {{ route('ui-tool-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <label for="inputCode">KODE (TLXXXX)</label>
                            @if(isset($data))
                                <input type="text" value="@if(isset($data)) {{ $data->code }} @endif" class="form-control @error('code') is-invalid @enderror" id="inputCode" name="code" required readonly>
                            @else
                                <input type="text" value="@if(isset($data)) {{ $data->code }} @endif" class="form-control @error('code') is-invalid @enderror" id="inputCode" name="code" required>
                            @endif
                            @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputName">Nama</label>
                            <input type="text" value="@if(isset($data)) {{ $data->name }} @endif" class="form-control @error('name') is-invalid @enderror" id="inputName" name="name" required>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputDescription">Deskripsi</label>
                            <input type="text" value="@if(isset($data)) {{ $data->description }} @endif" class="form-control @error('description') is-invalid @enderror" id="inputDescription" name="description">
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status </label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                @if(isset($data))
                                    @if($data->status == 0)
                                        <option value="0" selected>Mati</option>
                                        <option value="1">Hidup</option>
                                    @endif
                                    @if($data->status == 1)
                                        <option value="0">Mati</option>
                                        <option value="1" selected>Hidup</option>
                                    @endif
                                @else
                                    <option value="0">Mati</option>
                                    <option value="1">Hidup</option>
                                @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPlaceId">Pilih Tempat</label>
                                <select value="@if(isset($data)) {{ $data->place_id }} @endif" class="form-control @error('place_id') is-invalid @enderror" id="inputPlaceId" name="place_id">
                                    @foreach($place as $pl)
                                        @if(isset($data))
                                            @if($pl->id == $data->place_id)
                                                <option value="{{ $pl->id }}" selected>{{ $pl->name }}</option>
                                            @else
                                                <option value="{{ $pl->id }}">{{ $pl->name }}</option>
                                            @endif
                                        @else
                                            @if(isset($id))
                                                @if($pl->id == $id)
                                                    <option value="{{ $pl->id }}" selected>{{ $pl->name }}</option>
                                                @else
                                                    <option value="{{ $pl->id }}">{{ $pl->name }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $pl->id }}">{{ $pl->name }}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @error('place_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
