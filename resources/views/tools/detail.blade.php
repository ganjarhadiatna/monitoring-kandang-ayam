@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper" style="margin-bottom: 20px;">
                <div class="mr-auto">
                    <h3 style="margin-top: 6px;">{{ $title }}</h3>
                </div>
                <!-- <div class="ml-auto">
                    <a href="{{ route('ui-tool-create') }}" class="btn btn-primary">
                        <i class="fa fa-lw fa-plus"></i> Tambah Alat
                    </a>
                </div> -->
            </div>

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <?php 
                        $tab = null;
                        $details = 'nav-item nav-link';
                        $bizpars = 'nav-item nav-link';
                        $datas = 'nav-item nav-link';
                        $pane_details = 'tab-pane fade';
                        $pane_bizpars = 'tab-pane fade';
                        $pane_datas = 'tab-pane fade';
                        if (isset($_GET['tabs'])) { 
                            $tab = $_GET['tabs'];
                        }
                        switch ($tab) {
                            case 'detail':
                                $details = 'nav-item nav-link active';
                                $bizpars = 'nav-item nav-link';
                                $datas = 'nav-item nav-link';
                                $pane_details = 'tab-pane fade show active';
                                $pane_bizpars = 'tab-pane fade';
                                $pane_datas = 'tab-pane fade';
                                break;
                            case 'parameter':
                                $details = 'nav-item nav-link';
                                $bizpars = 'nav-item nav-link active';
                                $datas = 'nav-item nav-link';
                                $pane_details = 'tab-pane fade';
                                $pane_bizpars = 'tab-pane fade show active';
                                $pane_datas = 'tab-pane fade';
                                break;
                            case 'monitoring':
                                $details = 'nav-item nav-link';
                                $bizpars = 'nav-item nav-link';
                                $datas = 'nav-item nav-link active';
                                $pane_details = 'tab-pane fade';
                                $pane_bizpars = 'tab-pane fade';
                                $pane_datas = 'tab-pane fade show active';
                                break;
                            default:
                                $details = 'nav-item nav-link active';
                                $bizpars = 'nav-item nav-link';
                                $datas = 'nav-item nav-link';
                                $pane_details = 'tab-pane fade show active';
                                $pane_bizpars = 'tab-pane fade';
                                $pane_datas = 'tab-pane fade';
                                break;
                        }
                    ?>
                    <a class="{{ $details }}" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Detail</a>
                    <a class="{{ $bizpars }}" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Parameter ({{ $data->ttl_bizpars }})</a>
                    <a class="{{ $datas }}" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Monitoring ({{ $data->ttl_datas }})</a>
                </div>
            </nav>

            <div class="card" style="border-top: 0;">
                <div class="card-body" style="border-top: 0">


                    <div class="tab-content" id="nav-tabContent">
                        <div class="{{ $pane_details }}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div class="mr-auto">
                                    <h3 style="margin-top: 6px;">Detail</h3>
                                </div>
                                <div class="ml-auto">
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                                        <i class="fa fa-lw fa-trash-alt"></i> Hapus
                                    </button>
                                    <a href="{{ route('ui-tool-edit-by-id', [$id, $data->id]) }}" class="btn btn-warning">
                                        <i class="fa fa-lw fa-pencil-alt"></i> Edit
                                    </a>
                                </div>
                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel-{{ $data->id }}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header"> 
                                            <h5 class="modal-title" id="deleteModalLabel-{{ $data->id }}">Peringatan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Data akan dihapus secara permanen, lanjutkan?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                            <a class="btn btn-primary" 
                                                href="{{ route('ui-tool-delete') }}"
                                                onclick="event.preventDefault(); document.getElementById('id-form-{{ $data->id }}').submit();"
                                                >
                                                Lanjutkan
                                            </a>

                                            <form id="id-form-{{ $data->id }}" action="{{ route('ui-tool-delete') }}" method="POST" style="display: block;">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $data->id }}" />
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <b style="width: 200px;">KODE</b>
                                <b style="width: 30px;">:</b>
                                <b class="width-full">{{ $data->code }}</b>
                            </div>
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div style="width: 200px;">Nama alat</div>
                                <div style="width: 30px;">:</div>
                                <div class="width-full">{{ $data->name }}</div>
                            </div>
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div style="width: 200px;">Deskripsi</div>
                                <div style="width: 30px;">:</div>
                                <div class="width-full">{{ $data->description }}</div>
                            </div>
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div style="width: 200px;">Tempat</div>
                                <div style="width: 30px;">:</div>
                                <div class="width-full">{{ $data->place_name }}</div>
                            </div>
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div style="width: 200px;">Tanggal dibuat</div>
                                <div style="width: 30px;">:</div>
                                <div class="width-full">{{ $data->created_at }}</div>
                            </div>
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div style="width: 200px;">Tanggal diedit</div>
                                <div style="width: 30px;">:</div>
                                <div class="width-full">{{ $data->updated_at }}</div>
                            </div>
                            <div class="wrapper">
                                <div style="margin-top: 6px; width: 200px;">Status</div>
                                <div style="margin-top: 6px; width: 30px;">:</div>
                                <div class="width-full">
                                    @if($data->status == 0) 
                                        <button id="button-tool-{{ $data->id }}" class="btn btn-danger" onclick="setStatus({{ $data->id }})">
                                            <i id="button-icon-{{ $data->id }}" class="fa fa-lw fa-minus"></i>
                                            <span style="margin-left: 5px;" id="button-text-{{ $data->id }}">{{ $data->status ? 'Hidup' : 'Mati' }}</span>
                                        </button>
                                    @endif 
                                    @if($data->status == 1) 
                                        <button id="button-tool-{{ $data->id }}" class="btn btn-success" onclick="setStatus({{ $data->id }})">
                                            <i id="button-icon-{{ $data->id }}" class="fa fa-lw fa-check"></i>
                                            <span style="margin-left: 5px;" id="button-text-{{ $data->id }}">{{ $data->status ? 'Hidup' : 'Mati' }}</span>
                                        </button>
                                    @endif 
                                </div>
                            </div>
                        </div>


                        <div class="{{ $pane_bizpars }}" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div class="mr-auto">
                                    <h3 style="margin-top: 6px;">Parameter</h3>
                                </div>
                                <div class="ml-auto">
                                    <!-- <button class="btn btn-danger">
                                        <i class="fa fa-lw fa-trash-alt"></i> Hapus Semua
                                    </button> -->
                                    <a href="{{ route('ui-toolbizpar-create-by-id', [$data->place_id, $data->id]) }}" class="btn btn-primary">
                                        <i class="fa fa-lw fa-plus"></i> Tambah Parameter
                                    </a>
                                </div>
                            </div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">NO</th>
                                        <th scope="col">Key</th>
                                        <th scope="col">Value</th>
                                        <th scope="col">Dekripsi</th>
                                        <th scope="col">Tanggal Dibuat</th>
                                        <th scope="col">Tanggal Edit</th>
                                        <th width="120"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php $page_bizpars = isset($_GET['page_bizpars']) ? (($_GET['page_bizpars'] - 1) * 10) : 0; ?>
                                    @foreach($params as $dt)
                                        <tr>
                                            <th scope="row">{{ ($i + $page_bizpars) }}</th>
                                            <td>{{ $dt->key }}</td>
                                            <td>{{ $dt->value }}</td>
                                            <td>{{ $dt->description }}</td>
                                            <td>{{ $dt->created_at }}</td>
                                            <td>{{ $dt->updated_at }}</td>
                                            <td>
                                                <a href="{{ route('ui-bizpar-edit', $dt->id) }}" class="btn btn-warning">
                                                    <i class="fa fa-lw fa-pencil-alt"></i>
                                                </a>
                                                
                                                <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModalParameter">
                                                    <i class="fa fa-lw fa-trash-alt"></i>
                                                </button>

                                                <div class="modal fade" id="deleteModalParameter" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabelParameter-{{ $dt->id }}" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header"> 
                                                            <h5 class="modal-title" id="deleteModalLabelParameter-{{ $dt->id }}">Peringatan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Data akan dihapus secara permanen, lanjutkan?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <a class="btn btn-primary" 
                                                                href="{{ route('ui-bizpar-delete') }}"
                                                                onclick="event.preventDefault(); document.getElementById('id-form-parameter-{{ $dt->id }}').submit();"
                                                                >
                                                                Lanjutkan
                                                            </a>

                                                            <form id="id-form-parameter-{{ $dt->id }}" action="{{ route('ui-bizpar-delete') }}" method="POST" style="display: block;">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                            </form>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $params->appends(['tabs' => 'parameter'])->links() }}
                        </div>


                        <div class="{{ $pane_datas }}" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="wrapper" style="margin-bottom: 15px;">
                                <div class="mr-auto">
                                    <h3 style="margin-top: 6px;">Monitoring</h3>
                                </div>
                                <div class="ml-auto">
                                    <button class="btn btn-danger">
                                        <i class="fa fa-lw fa-trash-alt"></i> Bersihkan
                                    </button>
                                    <a href="{{ route('ui-data-create-by-id', [$data->place_id, $data->id]) }}" class="btn btn-primary">
                                        <i class="fa fa-lw fa-plus"></i> Tambah Monitoring
                                    </a>
                                </div>
                            </div>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">NO</th>
                                        <th scope="col">Nilai</th>
                                        <th scope="col">Dekripsi</th>
                                        <th scope="col">Parameter</th>
                                        <th scope="col">Tanggal Dibuat</th>
                                        <th scope="col">Tanggal Edit</th>
                                        <th width="120"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php $page_datas = isset($_GET['page_datas']) ? (($_GET['page_datas'] - 1) * 10) : 0; ?>
                                    @foreach($monitoring as $dt)
                                        <tr>
                                            <th scope="row">{{ ($i + $page_datas) }}</th>
                                            <td>{{ $dt->value }}</td>
                                            <td>{{ $dt->description }}</td>
                                            <td>{{ $dt->bizpar_name }}</td>
                                            <td>{{ $dt->created_at }}</td>
                                            <td>{{ $dt->updated_at }}</td>
                                            <td>
                                                <a href="{{ route('ui-data-edit', $dt->id) }}" class="btn btn-warning">
                                                    <i class="fa fa-lw fa-pencil-alt"></i>
                                                </a>
                                                
                                                <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModalMonitoring">
                                                    <i class="fa fa-lw fa-trash-alt"></i>
                                                </button>

                                                <div class="modal fade" id="deleteModalMonitoring" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabelMonitoring-{{ $dt->id }}" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header"> 
                                                            <h5 class="modal-title" id="deleteModalLabelMonitoring-{{ $dt->id }}">Peringatan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Data akan dihapus secara permanen, lanjutkan?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <a class="btn btn-primary" 
                                                                href="{{ route('ui-data-delete') }}"
                                                                onclick="event.preventDefault(); document.getElementById('id-form-monitoring-{{ $dt->id }}').submit();"
                                                                >
                                                                Lanjutkan
                                                            </a>

                                                            <form id="id-form-monitoring-{{ $dt->id }}" action="{{ route('ui-data-delete') }}" method="POST" style="display: block;">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                            </form>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $monitoring->appends(['tabs' => 'monitoring'])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function setStatus (id) {
        $.ajax({
            url: "{{ url('api/status') }}",
            dataType: 'json',
            type: 'POST',
            data: {
                'id': id
            }
        }).done(function(data) {
            console.log('data', data)
            var dt = data.status_tool;
            if (dt == '1') {
                $('#button-tool-' + id).removeClass('btn btn-danger').addClass('btn btn-success');
                $('#button-icon-' + id).removeClass('fa fa-lw fa-minus').addClass('fa fa-lw fa-check');
                $('#button-text-' + id).html('Hidup')
            } else {
                $('#button-tool-' + id).removeClass('btn btn-success').addClass('btn btn-danger');
                $('#button-icon-' + id).removeClass('fa fa-lw fa-check').addClass('fa fa-lw fa-minus');
                $('#button-text-' + id).html('Mati')
            }
        });
    }
</script>

@endsection
