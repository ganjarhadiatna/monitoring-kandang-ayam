@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">{{ $title }}</h3>
                    </div>
                    <div class="ml-auto">
                        <form mtehod="GET" action="{{ route('ui-lampu-search') }}" style="width: 500px; display: inline-block; vertical-align: top;">
                            <input id="search" type="search" class="form-control @error('search') is-invalid @enderror" name="search" value="{{ isset($_GET['search']) ? $_GET['search'] : old('searc') }}" required placeholder="Cari berdasarkan kode atau nama lampu.." autocomplete="search">
                        </form>
                        <a href="{{ route('ui-lampu-create') }}" class="btn btn-primary">
                            <i class="fa fa-lw fa-plus"></i>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">NO</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Lampu</th>
                                <th scope="col">Lokasi</th>
                                <th scope="col">Tegangan</th>
                                <th scope="col">Arus</th>
                                <th scope="col">Daya</th>
                                <th scope="col">Frekuensi</th>
                                <th scope="col">Status Baterai</th>
                                <th scope="col">Tanggal Dibuat</th>
                                <th scope="col">Status</th>
                                <th width="160"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <?php $map = "https://www.openstreetmap.org/#map=19/".$dt->longitude."/".$dt->latitude ?>
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->kode }}</td>
                                    <td>{{ $dt->nama }}</td>
                                    <td>
                                        <a href="{{ $map }}" target="_blank">
                                            {{ $dt->lokasi }} <i class="fa fa-1x fa-map-marker-alt" ></i>
                                        </a>
                                    </td>
                                    <td id="data-tegangan-{{ $dt->id }}">{{ $dt->tegangan > 0 ? $dt->tegangan : '0' }}</td>
                                    <td id="data-arus-{{ $dt->id }}">{{ $dt->arus > 0 ? $dt->arus : '0' }}</td>
                                    <td id="data-daya-{{ $dt->id }}">{{ $dt->daya > 0 ? $dt->daya : '0' }}</td>
                                    <td id="data-frekuensi-{{ $dt->id }}">{{ $dt->frekuensi > 0 ? $dt->frekuensi : '0' }}</td>
                                    <td id="data-faktor-{{ $dt->id }}">{{ $dt->daya_faktor > 0 ? $dt->daya_faktor : '0' }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>
                                        @if($dt->status == 0) 
                                            <button id="button-lampu-{{ $dt->id }}" class="btn btn-danger" onclick="setLampu({{ $dt->id }})">
                                                <i id="button-icon-{{ $dt->id }}" class="fa fa-lw fa-minus"></i>
                                            </button>
                                        @endif 
                                        @if($dt->status == 1) 
                                            <button id="button-lampu-{{ $dt->id }}" class="btn btn-success" onclick="setLampu({{ $dt->id }})">
                                                <i id="button-icon-{{ $dt->id }}" class="fa fa-lw fa-check"></i>
                                            </button>
                                        @endif 
                                    </td>
                                    <td>
                                        <a href="{{ route('ui-lampu-monitoring', $dt->id) }}" class="btn btn-primary">
                                            <i class="fa fa-lw fa-chart-line"></i>
                                        </a>
                                        
                                        <a href="{{ route('ui-lampu-edit', $dt->id) }}" class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </a>
                                        
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel-{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header"> 
                                                    <h5 class="modal-title" id="deleteModalLabel-{{ $dt->id }}">Peringatan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Lampu akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                    <a class="btn btn-primary" 
                                                        href="{{ route('ui-lampu-delete') }}"
                                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                        >
                                                        Lanjutkan
                                                    </a>

                                                    <form id="id-form-{{ $dt->id }}" action="{{ route('ui-lampu-delete') }}" method="POST" style="display: block;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>

<script>
    function setLampu (id) {
        $.ajax({
            url: "{{ url('api/status') }}",
            dataType: 'json',
            type: 'POST',
            data: {
                'id': id
            }
        }).done(function(data) {
            var dt = data.status_lampu;
            if (dt == '1') {
                $('#button-lampu-' + id).removeClass('btn btn-danger').addClass('btn btn-success');
                $('#button-icon-' + id).removeClass('fa fa-lw fa-minus').addClass('fa fa-lw fa-check');
            } else {
                $('#button-lampu-' + id).removeClass('btn btn-success').addClass('btn btn-danger');
                $('#button-icon-' + id).removeClass('fa fa-lw fa-check').addClass('fa fa-lw fa-minus');
            }
        });
    }

    function getMonitoring () {
        $.ajax({
            url: "{{ url('api/get') }}",
            dataType: 'json'
        }).done(function(data) {
            var dt = data.data;
            for (let index = 0; index < dt.length; index++) {
                const element = dt[index];
                $('#data-tegangan-' + element.id).text(element.tegangan > 0 ? element.tegangan : '0');
                $('#data-arus-' + element.id).text(element.arus > 0 ? element.arus : '0');
                $('#data-daya-' + element.id).text(element.daya > 0 ? element.daya : '0');
                $('#data-frekuensi-' + element.id).text(element.frekuensi > 0 ? element.frekuensi : '0');
                $('#data-faktor-' + element.id).text(element.daya_faktor > 0 ? element.daya_faktor : '0');
            }
        });
    }

    $(document).ready(function () {
        setInterval(() => {
            getMonitoring();
        }, 2000);
    });
</script>

@endsection
