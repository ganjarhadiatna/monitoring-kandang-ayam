@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Parameter</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Parameter</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-bizpar-update') }} @else {{ route('ui-bizpar-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <label for="inputKey">Key</label>
                            @if(isset($data))
                                <input type="text" value="@if(isset($data)) {{ $data->key }} @endif" class="form-control @error('key') is-invalid @enderror" id="inputKey" name="key" required readonly>
                            @else
                                <input type="text" value="@if(isset($data)) {{ $data->key }} @endif" class="form-control @error('key') is-invalid @enderror" id="inputKey" name="key" required>
                            @endif
                            @error('key')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputValue">Value</label>
                            <input type="text" value="@if(isset($data)) {{ $data->value }} @endif" class="form-control @error('value') is-invalid @enderror" id="inputValue" name="value" required>
                            @error('value')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputDescription">Deskripsi</label>
                            <input type="text" value="@if(isset($data)) {{ $data->description }} @endif" class="form-control @error('description') is-invalid @enderror" id="inputDescription" name="description">
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputToolId">Pilih Alat</label>
                                <select value="@if(isset($data)) {{ $data->tool_id }} @endif" class="form-control @error('tool_id') is-invalid @enderror" id="inputToolId" name="tool_id">
                                    @foreach($place as $pl)
                                        @if(isset($data))
                                            @if($pl->id == $data->tool_id)
                                                <option value="{{ $pl->id }}" selected>{{ $pl->name }}</option>
                                            @else
                                                <option value="{{ $pl->id }}">{{ $pl->name }}</option>
                                            @endif
                                        @else
                                            @if(isset($id))
                                                @if($pl->id == $id)
                                                    <option value="{{ $pl->id }}" selected>{{ $pl->name }}</option>
                                                @else
                                                    <option value="{{ $pl->id }}">{{ $pl->name }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $pl->id }}">{{ $pl->name }}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @error('tool_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
