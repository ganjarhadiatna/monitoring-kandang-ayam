<?php
    use App\Place;
    $places = Place::orderBy('id', 'asc')->get();
?>
<nav id="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <h3>{{ 'MONITORING' }}</h3>
        </div>

        <ul class="list-unstyled components">
            <li>
                <div class="space">Dashboard</div>
            <li>
            <li id="home">
                <a href="{{ route('home') }}">
                    <i class="fa fa-lg fa-home icon"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <div class="space">Master Data</div>
            <li>
            <li id="vitamin">
                <a href="#">
                    <i class="fa fa-lg fa-calendar-alt icon"></i>
                    Kelola Vitamin
                </a>
            </li>
            <!-- <li id="tool">
                <a href="{{ route('ui-tool') }}">
                    <i class="fa fa-lg fa-th-list icon"></i>
                    Kelola Alat
                </a>
            </li>
            <li>
                <a href="{{ route('ui-bizpar') }}" id="bizpar">
                    <i class="fa fa-lg fa-th-list icon"></i>
                    Kelola Parameter
                </a>
            </li> -->
            <li id="data">
                <a href="{{ route('ui-data') }}">
                    <i class="fa fa-lg fa-chart-line icon"></i>
                    Data Monitoring
                </a>
            </li>
            <li>
                <div class="space">Daftar Tempat</div>
            <li>
            @if(!empty($places))
                @foreach($places as $dt)
                    <li id="{{ 'place-' . $dt->id }}">
                        <a href="{{ route('ui-place-detail', $dt->id) }}" id="{{ $dt->id }}">
                            <i class="fa fa-lg fa-map-marker-alt icon"></i>
                            {{ $dt->name }}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</nav>

<script>
    $(document).ready(function () {
        var sdb = "{{ isset($sidebar) ? $sidebar : '' }}"
        console.log('sdb', sdb)
        if (sdb != '') {
            $('#' + sdb).addClass('active');
        }
    });
</script>